﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour {

    //Value to prevent sticking to high angle surfaces
    public float minGroundNormalY = .65f;
    public float gravityModifier = 1f;

    //Change value to move in direction
    protected Vector2 targetVelocity;
    protected bool isGrounded;
    //Current ground angle
    protected Vector2 groundNormal;
    protected Rigidbody2D rigidbody2d;
    protected Vector2 velocity;

    //Detect collisions and angle
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    //Detect movement
    protected const float minMoveDistance = 0.001f;

    //Collision buffer
    protected const float shellRadius = 0.01f;

    void OnEnable()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        //Set collision layer of gameObject
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
    }

    void Update()
    {
        //Refresh velocity
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {
        //Compute Velocity based on object

    }

    void FixedUpdate()
    {
        //modify falling physics
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        //modify horizontal movement
        velocity.x = targetVelocity.x;


        //reset isGrounded detection
        isGrounded = false;

        Vector2 deltaPosition = velocity * Time.deltaTime;


        //Horizontal Movement
        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        Vector2 move = moveAlongGround * deltaPosition.x;

        Movement(move, false);

        //Vertical Movement
        move = Vector2.up * deltaPosition.y;

        Movement(move, true);
    }

    void Movement(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;


        //check if moved
        if (distance > minMoveDistance)
        {
            int count = rigidbody2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {

                //Check normals

                Vector2 currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > minGroundNormalY)
                {
                    isGrounded = true;
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                //Slope check
                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0)
                {
                    velocity = velocity - projection * currentNormal;
                }

                //Collision buffer
                float modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }


        }

        rigidbody2d.position = rigidbody2d.position + move.normalized * distance;
    }
}
