﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlScript : PhysicsObject {

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    public Direction facing;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected override void ComputeVelocity()
    {

            Vector2 move = Vector2.zero;


            //Player Horizontal Movement
            move.x = Input.GetAxis("Horizontal");

            if (move.x > 0)
            {
                facing = Direction.Right;
                spriteRenderer.flipX = false;

            }
            else if (move.x < 0)
            {
                facing = Direction.Left;
                spriteRenderer.flipX = true;
            }


            //Jump
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = jumpTakeOffSpeed;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * 0.5f;
                }
            }


            targetVelocity = move * maxSpeed;


    }
}

public enum Direction{
    Left,
    Right,
    Center
}
