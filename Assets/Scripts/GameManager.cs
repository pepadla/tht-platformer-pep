﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public GameModel model;

    void Awake()
    {
        Application.targetFrameRate = 60;
        if (instance == null)
        {

            instance = this;

        }
        else if (instance != this)
        {

            Destroy(gameObject);

        }

        DontDestroyOnLoad(gameObject);
    }

    public void SelectPlayerClass(PlayerClass playerClass){
        Debug.Log("Triggered: " + playerClass);
        model.currentClass = playerClass;
        SceneManager.LoadScene("Root");
    }
}
