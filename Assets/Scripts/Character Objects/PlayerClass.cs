﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass: MonoBehaviour {
    public float moveSpeed;
    public float jumpHeight;

    public void SelectPlayerClass(){
        GameManager.instance.model.currentClass = this;
    }

}
