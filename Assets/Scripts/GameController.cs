﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject playerGameObject;

	private void Start()
	{
        PlayerControlScript playerControlScript = playerGameObject.GetComponent<PlayerControlScript>();

        playerControlScript.maxSpeed = GameManager.instance.model.currentClass.moveSpeed;
        playerControlScript.jumpTakeOffSpeed = GameManager.instance.model.currentClass.jumpHeight;
	}


}
