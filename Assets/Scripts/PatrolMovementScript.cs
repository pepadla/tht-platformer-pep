﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolMovementScript : PhysicsObject {

    public float patrolSpeed = 7;
    public Vector2 patrolDirection = Vector2.left;

	protected override void ComputeVelocity()
	{
        //Auto move forward
        targetVelocity = patrolSpeed * patrolDirection;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
        if (collision.gameObject.tag == "AboutFace")
        {
            //Switch direction on patrol limit
            patrolDirection = patrolDirection * -1;
        }
	}



}
